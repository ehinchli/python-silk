from setuptools import setup, find_packages

setup(
    name='uvmsilk',
    version='0.1.33',
    packages=find_packages(),
    python_requires='>=2.6',
    install_requires=['anyconfig', 'attrdict', 'requests', 'requests-unixsocket', 'sqlalchemy<1.2'],
)

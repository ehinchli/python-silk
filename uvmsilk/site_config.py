import pprint
import anyconfig
from configparser import ConfigParser
from attrdict import AttrDict
from schema import Schema, Use, Optional, And, Regex, Const, SchemaError
import os
import warnings
import re

import six
if six.PY2:
    from urlparse import urlparse
elif six.PY3:
    from urllib.parse import urlparse


class SilkConfig:
    """Definitions about the silk service"""

    config_path = '/silk'

    def __init__(self, **kwargs):
        self.software = {}
        software_config_file = self.config_path + '/software.yaml'
        self.software = AttrDict(anyconfig.load(software_config_file))


class SiteConfig:
    """Web site configuration"""

    def __init__(self, **kwargs):
        self.site_root = kwargs.get('site_root')
        self.config_file = self.site_root + '/.silk.ini'
        self.hostname = kwargs.get('hostname')
        self.username = kwargs.get('username')
        self.group = kwargs.get('group', None)
        self.silk_config = SilkConfig()
        self.load_preferences()

    def load_preferences(self):
        config = ConfigParser()
        try:
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                # This currently spits out a warning about ConfigParser
                config.read(self.config_file)
        except Exception:
            pass
        finally:
            #config = {s: dict(config.items(s)) for s in config.sections()}
            # make Python 2.6 compatible
            for s in config.sections():
                config[s] = dict(config.items(s))

        def not_too_long(s): return len(s) <= 1024
        def is_web_url(url): return url.scheme in ('http', 'https')
        def is_uvm_url(url): return url.netloc.endswith(tuple('.uvm.edu'))
        def is_just_path(url): return url.scheme == '' and url.netloc == ''

        def has_valid_hostname(url):
            if len(url.netloc) > 255:
                return False
            allowed = re.compile(r'(?!-)[A-Z\d-]{1,63}(?<!-)$', re.IGNORECASE)
            return all(allowed.match(x) for x in url.netloc.split('.'))

        def clamp(n, smallest, largest):
            return max(smallest, min(n, largest))

        string_type = six.string_types[0]
        downcase = str.lower if six.PY3 else unicode.lower

        general_schema = Schema({
            Optional('log-destination', default='vhost-directory'): And(string_type, Use(downcase), lambda s: s in ('vhost-directory', 'centralized')),
            Optional('max-request-length'): And(Use(int), Use(lambda n: max(n, 131072)), Use(lambda n: min(n, 1048576000))),
            Optional('redirect-insecure-http', default=True): And(Use(lambda s: s in ('true', 'yes', '1'))),
            Optional('request-timeout'): And(Use(int), Use(lambda n: max(n, 20)), Use(lambda n: min(n, 7200))),
            Optional('proxy-root'): And(not_too_long, Const(And(Use(urlparse), is_web_url, is_uvm_url, has_valid_hostname))),
            Optional('document-root'): And(string_type, Use(os.path.basename), lambda s: os.path.exists(self.site_root + '/' + s)),
            Optional('rewrite-proxied-remote-address', default=False): And(string_type, Use(downcase), Use(lambda s: s in ('true', 'yes', '1'))),
        }, ignore_extra_keys=True)
        if 'general' in config:
            while config['general'] and not hasattr(self, 'general'):
                try:
                    self.general = general_schema.validate(dict(config['general']))
                except SchemaError as e:
                    # remove bad key.  Unfortunately, we have to parse for it.
                    result = re.search('^Key \'([^\']+)\' error', e.autos[0])
                    key = result.group(1)
                    del config['general'][key]
        else:
            self.general = AttrDict(dict())

        app_schema = Schema({
            'type': And(string_type, Use(downcase), lambda s: s in ('golang', 'java', 'nodejs', 'python', 'rlang', 'ruby')),
            Optional('uri', default='/'): And(not_too_long, Use(urlparse), is_just_path, Use(lambda u: u.path)),
            Optional('preserve-uri-prefix', default=True): bool,
            Optional('root', default=self.site_root): string_type,
            Optional('document-root', default='public'): string_type,
            Optional('startup-script'): string_type,
            Optional(Regex(r'env\.\w+$')): string_type,
        }, ignore_extra_keys=True)
        app_sections = [k for k in config.keys() if re.match('app\s*[\.:]?\s*', k)]
        self.apps = []
        self.invalid_apps = []
        for app_section in app_sections:
            try:
                app_settings = app_schema.validate(dict(config[app_section]))
                app_settings['config_section'] = app_section

                #if 'uri' not in app_settings:
                #    app_settings['uri'] = '/'

                if 'root' in app_settings:
                    if not app_settings['root'].startswith('/'):
                        app_settings['root'] = self.site_root + '/' + app_settings['root']
                    app_settings['root'] = os.path.normpath(app_settings['root'])
                    if not app_settings['root'].startswith(self.site_root):
                        raise Exception('app root path %s is outside site root' % app_settings['root'])
                    if not os.path.exists(app_settings['root']):
                        raise Exception('app root path %s does not exist' % app_settings['root'])
                else:
                    app_settings['root'] = self.site_root

                if 'document-root' in app_settings:
                    app_settings['document-root'] = os.path.normpath(app_settings['document-root'])
                    docroot_fullpath = os.path.normpath(app_settings['root'] + '/' + app_settings['document-root'])
                else:
                    app_settings['document-root'] = 'public'
                    docroot_fullpath = os.path.normpath(app_settings['root'] + '/' + app_settings['document-root'])
                if not docroot_fullpath.startswith(app_settings['root'] + '/'):
                    raise Exception('app document root path %s is outside app root' % docroot_fullpath)
                if app_settings['type'] not in ['rlang'] and not os.path.exists(docroot_fullpath):
                    raise Exception('app document root path %s does not exist' % docroot_fullpath)

                if app_settings['type'] not in ['rlang']:
                    if 'startup-script' not in app_settings:
                        raise Exception('startup-script is required for this app type')
                    app_settings['startup-script'] = os.path.normpath(app_settings['startup-script'])
                    #startupscript_fullpath = os.path.normpath(app_settings['root'] + '/' + app_settings['startup-script'])

                app_keys = list(app_settings.keys())
                app_settings['environment'] = {}
                for key in app_keys:
                    if key.startswith('env.'):
                        newkey = key[4:]
                        app_settings['environment'][newkey.upper()] = app_settings.pop(key)

                self.apps.append(AttrDict(**app_settings))
            except Exception as e:
                pprint.pprint(e)
                self.invalid_apps.append(AttrDict({'config_section': app_section}))

        java_schema = Schema({
            Optional('version', default=self.silk_config.software.java.default): And(Use(float), lambda f: f in self.silk_config.software.java.versions),
        })
        python_schema = Schema({
            Optional('version', default=self.silk_config.software.python.default): And(Use(float), lambda f: f in self.silk_config.software.python.versions),
        })
        ruby_schema = Schema({
            Optional('version', default=self.silk_config.software.ruby.default): And(Use(float), lambda f: f in self.silk_config.software.ruby.versions),
        })
        for lang, schema in {'java': java_schema, 'python': python_schema, 'ruby': ruby_schema}.items():
            try:
                setattr(self, lang, AttrDict(schema.validate(dict(config[lang]))))
            except Exception:
                setattr(self, lang, AttrDict({'version': self.silk_config.software[lang]['default']}))

    def network_port(self):
        network_port_file = "/usr/lib/unit-user-{username}/network_port".format(username=self.username)
        with open(network_port_file) as x:
            network_port = x.read()
        return int(network_port)

    def app_by_uri(self, uri):
        for app in self.apps:
            if uri == app.uri:
                return app

    def to_json(self):
        data = self.__dict__
        data['silk_config'] = data['silk_config'].__dict__
        return AttrDict(data)

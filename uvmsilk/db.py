import re
from sqlalchemy import create_engine, func, Column, DateTime, Enum, ForeignKey, Integer, String
from sqlalchemy.orm import relationship
from sqlalchemy.orm.collections import attribute_mapped_collection
from sqlalchemy.ext.declarative import as_declarative, declared_attr
from sqlalchemy.orm.exc import NoResultFound


@as_declarative()
class Base(object):
    @declared_attr
    def __tablename__(cls):
        str = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', cls.__name__)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', str).lower()

    __table_args__ = {'mysql_engine': 'InnoDB'}
    id = Column(Integer, primary_key=True)


class HostingServer(Base):
    name = Column(String(255))
    status = Column(Enum('production', 'development', 'retired', 'inactive'))
    service_id = Column(Integer, ForeignKey('hosted_service.id'))
    service = relationship('HostedService', lazy='joined', back_populates='servers')


class HostedService(Base):
    name = Column(String(10))
    accounts = relationship('HostedAccount', back_populates='service')
    servers = relationship('HostingServer', back_populates='service')


class HostedUser(Base):
    username = Column(String(10))
    date_added = Column(DateTime, default=func.now())
    accounts = relationship('HostedAccount',
                            collection_class=attribute_mapped_collection('service.name'),
                            lazy='joined',
                            back_populates='user')

    def __cmp__(self, other):
        if hasattr(other, 'username'):
            if self.username > other.username:
                return 1
            elif self.username < other.username:
                return -1
            else:
                return 0


class HostedAccount(Base):
    user_id = Column(Integer, ForeignKey('hosted_user.id'))
    user = relationship("HostedUser", lazy='joined', back_populates='accounts')
    service_id = Column(Integer, ForeignKey('hosted_service.id'))
    service = relationship('HostedService', lazy='joined', back_populates='accounts')
    date_added = Column(DateTime, default=func.now())
    options = relationship('HostedAccountOption',
                           collection_class=attribute_mapped_collection('name'),
                           cascade='all',
                           lazy='joined',
                           back_populates='account')
    virtualhosts = relationship('VirtualHost',
                                collection_class=attribute_mapped_collection('hostname'),
                                cascade='all',
                                lazy='joined',
                                back_populates='owned_by_account')


class HostedAccountOption(Base):
    account_id = Column(Integer, ForeignKey('hosted_account.id'))
    account = relationship('HostedAccount', lazy='joined', back_populates='options')
    name = Column(String(30))
    value = Column(String(255))

    def __str__(self):
        return '%s = %s' % (self.name, self.value)


class VirtualHost(Base):
    owned_by_account_id = Column(Integer, ForeignKey('hosted_account.id'))
    owned_by_account = relationship('HostedAccount', lazy='joined', back_populates='virtualhosts')
    hostname = Column(String(255))
    site_root = Column(String(255))
    document_root = Column(String(255))
    log_directory = Column(String(255))
    aliases = relationship('VirtualHostAlias',
                           cascade='all',
                           lazy='joined',
                           back_populates='virtualhost')
    options = relationship('VirtualHostOption',
                           collection_class=attribute_mapped_collection('name'),
                           cascade='all',
                           lazy='joined',
                           back_populates='virtualhost')
    paths = relationship('VirtualHostPath',
                           cascade='all',
                           lazy='joined')

    def __cmp__(self, other):
        if hasattr(other, 'hostname'):
            if self.hostname > other.hostname:
                return 1
            elif self.hostname < other.hostname:
                return -1
            else:
                return 0


class VirtualHostAlias(Base):
    virtualhost_id = Column(Integer, ForeignKey('virtual_host.id'))
    virtualhost = relationship('VirtualHost', lazy='joined', back_populates='aliases')
    hostname = Column(String(255))

    def __str__(self):
        return self.hostname

class VirtualHostOption(Base):
    virtualhost_id = Column(Integer, ForeignKey('virtual_host.id'))
    virtualhost = relationship('VirtualHost', lazy='joined', back_populates='options')
    name = Column(String(30))
    value = Column(String(255))

    def __str__(self):
        return '%s = %s' % (self.name, self.value)

class VirtualHostPath(Base):
    virtualhost_id = Column(Integer, ForeignKey('virtual_host.id'))
    path = Column(String(255))
    options = relationship('VirtualHostPathOption',
                           collection_class=attribute_mapped_collection('name'),
                           cascade='all',
                           lazy='joined')

    def __str__(self):
        return self.path

class VirtualHostPathOption(Base):
    path_id = Column(Integer, ForeignKey('virtual_host_path.id'))
    name = Column(String(30))
    value = Column(String(255))

    def __str__(self):
        return '%s = %s' % (self.name, self.value)
